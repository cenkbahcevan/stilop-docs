var stilopQues = {
    		"value": "Are the shoulders and hips very similar width?",
    		"children": [{
    			"value": "Is there a defined waist?",
    			"children": [{
    				"value": "Hourglass",
    				"children": []
    			}, {
    				"value": "Does the body appear slim and straight through the torso?",
    				"children": [{
    					"value": "Rectangle",
    					"children": []
    				}, {
    					"value": "Does the body appear straight and more broad?",
    					"children": [{
    						"value": "Rectangle",
    						"children": []
    					}, {
    						"value": "Oval",
    						"children": []
    					}]
    				}]
    			}]
    		}, {
    			"value": "Are the hips narrower than the shoulders or bust?",
    			"children": [{
    				"value": "Are the shoulders broader and more prominent than the hips or tummy?",
    				"children": [{
    					"value": "Reverse Rectangle",
    					"children": []
    				},{
    					"value": "Oval",
    					"children": []
    				}]
    			}, {
    				"value": "Is the widest part at the high hip just under the waist and appears like a shelf?",
    				"children": [{
    					"value": "Hourglass",
    					"children": []
    				},{
    					"value": "Triangle",
    					"children": []
    				}]
    			}]
    		}]
    	}

    	tempDict = []

      var botui = new BotUI('my-botui-app');
      var timer = setInterval(function(){}, 1000)


      var firstContent = "![firstimg](https://www.avicena.ist/static/images/id_607.png) Omuzlarınız ve kalçanız eşit genişlikte mi?"

      botui.message.bot({
      	delay:700,
      	content:"Merhaba, <b>vücut şeklinize uygun</b> ürünleri sizin için listeyelebilirim. <br> </br> Aşağıdaki birkaç soruyu yanıtlayarak bana yardımcı olabilirsiniz. 😊"
      }).then(() => {
      	return botui.message.bot({
      		delay:700,
      		content: firstContent
      	}).then (function () {
      		return botui.action.button({
      			delay: 1000,
      			action: [{
      				text: "Evet",
      				value: 0
      			},{
      				text: "Hayır",
      				value: 1
      			}]
      		})
      	}).then( res => {
      		//alert(res.value)
      		timer = setInterval(setTime2, 1000);
      		if (res.value == 0){
      			tempDict = stilopQues.children[0]
      			applyAdding(botui, "Evet")
      		}
      		else{
      			tempDict = stilopQues.children[1]
      			applyAdding(botui, "Hayır")
      		}
      	})
      })

window.setInterval(function() {

}, 100);

$(document).ready(function () {
  setTimeout(function () {
    document.getElementById("my-botui-app").style = "max-height:400px";
  }, 3000);
});

var userCookie = document.cookie
var initOK = true
var currentQuestionNo = 607
var startFlag = true
function applyAdding(botui,answer){



   getQuestions(currentQuestionNo, answer, document.cookie)
   setTimeout(function () {
     addMessage(botui, answer);
   }, 2000)





}
function addMessage(botui, answer) {





        var resultDict = result


    //alert("bak _> " + JSON.stringify(resultDict))


	if (resultDict.status == "Bitti"){
		clearInterval(timer)
		return botui.message.bot({
			loading:true,
			delay:700,
			content: "Done! Thank you for answering the questions."
		}).then(() => {
			botui.message.bot({
				loading: true,
				delay: 700,
				content: "Based on the answers you gave, your body type is: " + dict.value
			})
		})
	}
	else{
	  //alert("geldim" + resultDict.next_question)

	    if (resultDict.answers.length > 0) {
	      addNewText(botui,resultDict.next_question).then(() => {
	        addActions(botui)
	      })
	    }else{
	      return botui.message.bot({
	        loading: true,
	        delay: 700,
	        content: "Teşekkürler. 😊 Verdiğiniz cevaplara göre vücut şekliniz"
	      }).then(() => {
	        var finalShape = ""
	        if (resultDict.next_question == "Oval"){ finalShape = "oval.png"}
	        else if (resultDict.next_question == "Kum saati"){finalShape = "kumsaati.png"}
	        else if (resultDict.next_question == "Dikdörtgen"){finalShape = "ucgen.png"}
	        else if (resultDict.next_question == "Üçgen"){finalShape = "dikdortgen.png"}
	        else if (resultDict.next_question == "Ters üçgen"){finalShape = "tersucgen.png"}

	        addNewText(botui, "![body](https://www.avicena.ist/static/images/" + finalShape + ") <b>" + resultDict.next_question + "</b>").then(() => {
	          askIfTrueShape(botui, resultDict.next_question)
	        })
	      })
	    }
	}
}

function askIfTrueShape(botui, bShape){

  return botui.message.bot({
    loading: true,
    delay: 700,
    content: "Vücudunuz " + bShape + "tanımına uyuyor mu?"
  }).then(() => {
    return botui.action.button({
      			delay: 1000,
      			action: [{
      				text: "Evet",
      				value: 0
      			},{
      				text: "Hayır",
      				value: 1
      			}]
      }).then( res => {
      		//alert(res.value)
      		//timer = setInterval(setTime2, 1000);
      		clearInterval(timer)
	        var secondsPassed = parseInt($("#minutes2").text()) * 60 + parseInt($("#seconds2").text());
      		if (res.value == 0){
	          sendEndInfo(secondsPassed, bShape, document.cookie, "True")
      		}
      		else{
      		  sendEndInfo(secondsPassed, bShape, document.cookie, "False")
      		}
      	})
  })
}

//function addActionDict(answers){
//actions_dict = []
//answer_no = 0
//for answer in answers {
//current_dict = {}
//current_dict.text = answer
//current_dict.value = answer_no;
//answer_no ++
//answers_dict.append
//}

function addActions(botui) {
	return botui.action.button({
      			delay: 1000,
      			action: [{
      				text: "Evet",
      				value: 0
      			},{
      				text: "Hayır",
      				value: 1
      			}]
      		}).then(res => {
		  if (res.value == 0) {

		    applyAdding(botui,"Evet")
		  }
		  else{
		    applyAdding(botui,"Hayır")
		  }
		})
}

function addNewText(botui, myDict) {


  end = myDict
  first_part = "![image]("
  second_part = myDict.image + ")"
  third_part = myDict
  end = first_part + second_part + third_part + "\""
  if (myDict.image == null){
    end = myDict
  }
  return botui.message.bot({
    loading: true,
    delay: 700,
    content: end
   })

}

function sendEndInfo(chatTime, bodyShape, userCookie, isBShapeTrue) {

var $crf_token = $('[name="csrfmiddlewaretoken"]').attr('value');

	data = {

		"chat_time": chatTime,
		"body_shape": bodyShape,
		"cookie": userCookie,
		"isBodyShapeTrue": isBShapeTrue,
        "csrfmiddlewaretoken": document.cookie.substr(10)
	};

$.ajax({
      url:'/sendToEndPoint',
      type:'POST',
      data: JSON.stringify(data),
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      success:function(res){
        //alert("it works!");
        //alert(JSON.stringify(res))
        result = res
        currentQuestionNo = result.next_question_id
        currentQuestionNo = result.next_question_id
        return result
      },
      error:function(res){
        //alert(JSON.stringify(res))
        //alert("Bad thing happend! " + res.statusText);
      }
    })

}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

var result = {}
 function getQuestions(questionId, answerText, userCookie){

    var $crf_token = $('[name="csrfmiddlewaretoken"]').attr('value');
	data = {

		"question_id": questionId,
		"answer": answerText,
		"cookie": userCookie,
        "csrfmiddlewaretoken": document.cookie.substr(10)
	};




    $.ajax({
      url:'/nextQuestion',
      type:'POST',
      data: JSON.stringify(data),
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      success:function(res){
        //alert("it works!");
        //alert(JSON.stringify(res))
        result = res
        currentQuestionNo = result.next_question_id
        currentQuestionNo = result.next_question_id
        return result
      },
      error:function(res){
        //alert(JSON.stringify(res))
        //alert("Bad thing happend! " + res.statusText);
      }
    }).done(function() {
      //alert("Bittil")
      initOK = true
      return result
      });

    return result

}


var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var minutes2Label = document.getElementById("minutes2");
var seconds2Label = document.getElementById("seconds2");

var totalSeconds = 0;
var totalSeconds2 = 0;

setInterval(setTime, 1000);

function setTime() {
  ++totalSeconds;
  secondsLabel.innerHTML = pad(totalSeconds % 60);
  minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
}

function setTime2() {
	++totalSeconds2;
	seconds2Label.innerHTML = pad(totalSeconds2 % 60);
	minutes2Label.innerHTML = pad(parseInt(totalSeconds2 / 60));
}

function pad(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}


function showWindow(){
  document.getElementById("my-botui-app").style = "max-height:400px";
}