#Stilop yerleştirme dökümantasyonu

Altta bulunan kod bloğunu sitenize koyarak bot arayüzünü sitenize yerleştirebilirsiniz. Javascript/CSS dosyalarını bu reposi
tory adresinden indirebilirsiniz.

### <head> kısmı dahil edilecek kodlar

```
<link rel="stylesheet" href="https://www.avicena.ist/static/css/stilop.css" />
<link rel="stylesheet" href="https://www.avicena.ist/static/css/botui.min.css" />
<link rel="stylesheet" href="https://www.avicena.ist/static/css/botui-theme-default.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
```

Body kısmında botu gösterecek kodlarımız


```
<div id="stilop-dialog">

     <div class="container" id="stilop-bot-app" style="display:none; z-index: 1000">
     
         <div style="bottom:220px;right:40px;position:fixed;">
             <div class="botui-app-container" style="max-height: 0px;" id="my-botui-app" >
                <div id="stilop-top-bar" onClick= "showWindow()">
                    <div class="row">
                        <div class="col-10">
                            <img id="stilop-logo" width = "110" height= "29" src="https://avicena.ist/static/images/stilop.png">
                        </div>
                        <div class="col-2">
                            <button onclick="hideShow()"  style="border: 0; background: transparent; margin-top: 10px;">
                                <img src="https://avicena.ist/static/images/cross.png" width="25" height="25" alt="submit" />
                            </button>
                        </div>
                    </div>
                </div>
                <bot-ui id="stilop-bot-ui"></bot-ui>
            </div>
        </div>
    </div>

    <button onclick="hideShow()" id="stiloButton" class="stilop-button-float" type="reset" title=""></button>

</div>

<script src="https://cdn.jsdelivr.net/vue/2.0.5/vue.min.js"></script>
<script src="https://avicena.ist/static/js/botui.min.js"></script>
<script src="https://avicena.ist/static/js/stilop.js"></script>
```
